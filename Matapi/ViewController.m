//
//  ViewController.m
//  Matapi
//
//  Created by dronnefjord on 2015-03-09.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "ViewController.h"
#import "TableViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchQuery;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (nonatomic) BOOL animate;
@property (nonatomic) NSTimer* timer;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.animate = YES;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(animateImage) userInfo:nil repeats:YES];
}

-(void)animateImage{
    if(self.animate){
        self.logo.frame = CGRectMake(0,72,200,100);
        self.animate = NO;
        [UIView animateWithDuration:3.0 animations:^{
            self.logo.frame = CGRectMake(self.view.frame.size.width-200,72,200,100);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:3.0 animations:^{
                self.logo.frame = CGRectMake(0,72,200,100);
            } completion:^(BOOL finished) {
                self.animate = YES;
            }];
        }];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"Text changed to: %@", searchText);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    self.animate = NO;
    [self.timer invalidate];
    self.timer = nil;
    
    TableViewController *controller = [segue destinationViewController];
    
    if([sender tag] == 1){
        NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@", self.searchQuery.text];
        NSURL *url = [NSURL URLWithString:searchString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSError *parseError;
            
            NSArray *food = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            dispatch_async(dispatch_get_main_queue(), ^{
                controller.data = food;
                [controller reloadDataInTableView];
            });
        }];
        [task resume];
    } else if([sender tag] == 2){
        NSURL *url = [NSURL URLWithString:@"http://matapi.se/foodstuff"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSError *parseError;
            
            NSArray *food = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            dispatch_async(dispatch_get_main_queue(), ^{
                controller.data = food;
                [controller reloadDataInTableView];
            });
        }];
        [task resume];
    }
}

@end
