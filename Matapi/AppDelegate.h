//
//  AppDelegate.h
//  Matapi
//
//  Created by dronnefjord on 2015-03-09.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

