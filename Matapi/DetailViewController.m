//
//  DetailViewController.m
//  Matapi
//
//  Created by dronnefjord on 2015-03-18.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nutrientValuesLabel;
@property (weak, nonatomic) IBOutlet UILabel *kcalLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbsLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *fibreLabel;
@property (weak, nonatomic) IBOutlet UILabel *natriumLabel;
@property (weak, nonatomic) IBOutlet UILabel *vitCLabel;
@property (weak, nonatomic) IBOutlet UILabel *vitDLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalValueLabel;

@property (weak, nonatomic) IBOutlet UIButton *collapseButton;

@property (weak, nonatomic) IBOutlet UILabel *kcalValue;
@property (weak, nonatomic) IBOutlet UILabel *proteinValue;
@property (weak, nonatomic) IBOutlet UILabel *carbValue;
@property (weak, nonatomic) IBOutlet UILabel *fatValue;
@property (weak, nonatomic) IBOutlet UILabel *fiberValue;
@property (weak, nonatomic) IBOutlet UILabel *natriumValue;
@property (weak, nonatomic) IBOutlet UILabel *vitCValue;
@property (weak, nonatomic) IBOutlet UILabel *vitDValue;
@property (weak, nonatomic) IBOutlet UILabel *totalValue;

@property (nonatomic) BOOL loaded;
@property (nonatomic) BOOL detailsCollapsed;

@property (nonatomic) UIDynamicAnimator* animator;
@property (nonatomic) UICollisionBehavior* collisions;
@property (nonatomic) UIGravityBehavior* gravity;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self resetValues];
    self.detailsCollapsed = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)resetValues{
    if(!self.loaded){
        NSString *initString = @"0";
        self.kcalValue.text = initString;
        self.proteinValue.text = initString;
        self.carbValue.text = initString;
        self.fatValue.text = initString;
        self.fiberValue.text = initString;
        self.natriumValue.text = initString;
        self.vitCValue.text = initString;
        self.vitDValue.text = initString;
        self.totalValue.text = initString;
    }
}
-(void)reloadData{
    self.loaded = YES;
    self.title = [self.detailData objectForKey:@"name"];
    
    self.kcalValue.text = [NSString stringWithFormat:@"%@ / %@", [self getValueWithKey:@"energyKj"],[self getValueWithKey:@"energyKcal"]];
    self.proteinValue.text = [self getValueWithKey:@"protein"];
    self.carbValue.text = [self getValueWithKey:@"carbohydrates"];
    self.fatValue.text = [self getValueWithKey:@"fat"];
    self.fiberValue.text = [self getValueWithKey:@"fibres"];
    self.natriumValue.text = [self getValueWithKey:@"salt"];
    self.vitCValue.text = [self getValueWithKey:@"vitaminC"];
    self.vitDValue.text = [self getValueWithKey:@"vitaminD"];
    
    self.totalValue.text = [self getTotalValue];
    
}

- (IBAction)collapse:(id)sender {
    [self collapseDetails];
}

-(void)collapseDetails{
    if(!self.detailsCollapsed){
        NSArray* items = @[self.nutrientValuesLabel, self.kcalLabel, self.proteinLabel, self.carbsLabel, self.fatLabel, self.fibreLabel, self.natriumLabel, self.vitCLabel, self.vitDLabel, self.totalValueLabel, self.kcalValue, self.proteinValue, self.natriumValue, self.fiberValue, self.fatValue, self.totalValue, self.vitCValue, self.vitDValue, self.carbValue, self.collapseButton];
        self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
        self.gravity = [[UIGravityBehavior alloc] initWithItems:items];
        self.collisions = [[UICollisionBehavior alloc] initWithItems:items];
        self.collisions.translatesReferenceBoundsIntoBoundary = YES;
        
        [self.animator addBehavior:self.gravity];
        [self.animator addBehavior:self.collisions];
    }
}

-(NSString*)getValueWithKey:(NSString*)key{
    NSDictionary *nutrients = [self.detailData objectForKey:@"nutrientValues"];
    return [NSString stringWithFormat:@"%@", [nutrients objectForKey:key]];
}
-(NSString*)getTotalValue{
    NSDictionary *nutrients = [self.detailData objectForKey:@"nutrientValues"];
    int value =
        [[nutrients objectForKey:@"energyKj"] intValue] +
        [[nutrients objectForKey:@"energyKcal"] intValue] +
        [[nutrients objectForKey:@"protein"] intValue] +
        [[nutrients objectForKey:@"carbohydrates"] intValue] +
        [[nutrients objectForKey:@"fat"] intValue] +
        [[nutrients objectForKey:@"fibres"] intValue] +
        [[nutrients objectForKey:@"salt"] intValue] +
        [[nutrients objectForKey:@"vitaminC"] intValue] +
        [[nutrients objectForKey:@"vitaminD"] intValue];
    return [NSString stringWithFormat:@"%d", value];
}

@end
