//
//  TableViewController.m
//  Matapi
//
//  Created by dronnefjord on 2015-03-16.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "TableViewController.h"
#import "DetailViewController.h"

@interface TableViewController ()
@property (strong, nonatomic) IBOutlet UITableView *table;
@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)reloadDataInTableView{
    [self.table reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    cell.textLabel.text = [[self.data objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UITableViewCell *cell = sender;
    NSIndexPath *index = [self.table indexPathForCell:cell];
    
    DetailViewController *controller = [segue destinationViewController];
    
    NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", [[self.data objectAtIndex:index.row] objectForKey:@"number"]];

    NSURL *url = [NSURL URLWithString:searchString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSError *parseError;
        
        NSDictionary *detailedFood = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        dispatch_async(dispatch_get_main_queue(), ^{
            controller.detailData = detailedFood;
            [controller reloadData];
        });
    }];
    [task resume];
}

@end

