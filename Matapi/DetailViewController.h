//
//  DetailViewController.h
//  Matapi
//
//  Created by dronnefjord on 2015-03-18.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (nonatomic) NSDictionary *detailData;
-(void)reloadData;
@end
