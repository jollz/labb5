//
//  TableViewController.h
//  Matapi
//
//  Created by dronnefjord on 2015-03-16.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController
@property (nonatomic) NSArray* data;

-(void)reloadDataInTableView;
@end
